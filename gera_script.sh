#!/usr/bin/env bash

# Opções de editor, aqui usuário define
# o seu editor preferido
editor="gedit"

# Variaveis do programa
dia_de_hoje=$( date +'%d/%m/%Y' )

# Cabeçalho
header="#!/usr/bin/env bash
#---------------------------------------------------------------------
# Script    :
# Descrição :
# Autor     : 

# Testa se usuário passou o número certo de argumentos.

[[ $# -ne 1 ]] && echo "Você precisa de um nome para o script" && exit 1

# Testar se arquivo existe
[[ -f $1 ]] && echo "Arquivo existe!... Saindo!" && exit 1


echo "$header" > $1
chmod +x $1
$editor $1 &

exit 0

