#!/usr/bin/env bash
# Script que mostra informações do sistema

clear

echo ""
echo " INFORMAÇÕES DO SISTEMA!"
echo "----------------------------------------"

echo -n "Usuário : "
whoami

echo -n "Hostname : "
hostname

echo -n "Uptime : "
uptime -p

echo -n "Kernel : "
uname -rms

echo "-----------------------------------------"
echo ""