#!/usr/bin/env bash

# Curso Shell Script: debxp
# Professor: Blau Araújo
# Script para testar PID
# Descrição: Script para testar o PID das 
# sessões do bash no terminal e no script

#=================================================================#
#                       Início do programa
#=================================================================#

clear

echo "COMPARANDO PID'S DAS SESSÕES DO BASH NO SCRIPT E NO TERMINAL"
echo "------------------------------------------------------------"
echo "PID da sessão do Bash no terminal : $PPID"
echo "PID da sessão do Bash neste script: $$"
echo "------------------------------------------------------------"

ps -p $$,$PPID -o pid=,user=,tty=,args=

exit